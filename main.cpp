/* 
 * File:   main.cpp
 * Author: david
 *
 * Created on September 22, 2015, 8:14 PM
 */

#include <cstdlib>
#include <random>
#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <cmath>
#include <limits>
#include <chrono>
#include "matplotpp.h"

using namespace std;
void printDoubleArray(double* arr, int n);
double* xAxisMean(double** arr, int xn, int yn);

// construct a trivial random generator engine from a time-based seed:
unsigned seed = chrono::system_clock::now().time_since_epoch().count();
default_random_engine generator(seed);

/*
 * 
 */
class Bandit {
    double qStars[7] = {-3.0, -2.0, -1.0, 0.0, 1.0, 2.0, 3.0};
    normal_distribution<double> gaussians[7];

public:
    const int n = 7;

    Bandit() {
        //init gaussian for each arm value
        for (int i = 0; i < n; i++) {
            gaussians[i] = normal_distribution<double>(qStars[i], 1.0);
        }
    }

    double reward(int armIndex) {
        //        return qStars[armIndex];
        //        default_random_engine generator;
        return gaussians[armIndex](generator);
    }

    //for performance test

    int getOptimalAction() {
        return distance(qStars, max_element(qStars, qStars + n));
    }
};

class Agent {
    double epsilon;
    double alpha;
    Bandit *bandit;
    int plays;
    double *Qs;

public:
    double *rewards;
    double *actions;
    double *optimalActions;

    Agent(Bandit bandit, int plays, double epsilon = NAN, double alpha = NAN, double tau = NAN, double initQ = 0) {
        this->bandit = &bandit;
        this->plays = plays;
        this->epsilon = epsilon;
        this->alpha = alpha;
        this->rewards = new double[plays];
        //        this->actions = new double[plays];
        this->optimalActions = new double[plays];
        this->Qs = new double[bandit.n];
        //init Q
        for (int i = 0; i < bandit.n; i++) {
            Qs[i] = initQ;
        }
    }

    void play() {

        for (int i = 0; i < plays; i++) {

            int action = chooseAction(0);
            double reward = bandit->reward(action);

            //Update action-value 
            updateActionValue(1, action, reward);

            //Log performance
            this->rewards[i] = reward;
            //            this->actions[i] = action;
            double correct = (action == bandit->getOptimalAction()) * 100;
            optimalActions[i] = correct;
//            cout << ", reward-Q-action: " << reward << ", " << Qs[action] << ", " << action << endl;
//            cout << "Qs: ";
//            printDoubleArray(Qs, 7);
//            cout << "correct:" << correct << endl;
        }
    }

    void reset() {

    }

protected:

    int chooseAction(int type) {
        if (type == 0)
            return chooseGreedyAction();
        else
            return chooseSoftmaxAction();
    }

    int chooseGreedyAction() {
        int action;
        //explore
        if (((double) rand() / RAND_MAX) < epsilon) {
            action = rand() % (bandit->n);
            cout << " explore: ";
        } else { //exploit
            action = distance(Qs, max_element(Qs, Qs + bandit->n));
            cout << " exploit ";
        }

        return action;
    }

    int chooseSoftmaxAction() {
        return 1;
    }

    void updateActionValue(int type, int action, double reward) {
        if (type == 0)
            return updateSampleAverageActionValue(action, reward);
        else
            return updateConstantAlphaActionValue(action, reward);
    }

    void updateSampleAverageActionValue(int action, double reward) {
        this->Qs[action] += this->alpha * (reward - this->Qs[action]);
    }

    void updateConstantAlphaActionValue(int action, double reward) {
        this->Qs[action] += this->alpha * (reward - this->Qs[action]);
    }

};

void softmax() {

}

tuple<double**, double**> runBanditExperience(int expNo, int plays, double epsilon = NAN, double alpha = NAN, double tau = NAN, double initQ = 0) {
    double **averageRewards = new double*[expNo];
    //    double **percentageOptimals = new *double[expNo];
    double **percentageOptimals = new double*[expNo];
    //    for(int j = 0; j < expNo; j++) {
    //        averageRewards[j] = new double[plays];
    //    }

    for (int i = 0; i < expNo; i++) {
        Bandit bandit = Bandit();
        Agent agent = Agent(bandit, plays, epsilon, alpha, tau, initQ);
        agent.play();
        //        averageRewards[i] = agent.rewards;
        averageRewards[i] = agent.rewards;
        percentageOptimals[i] = agent.optimalActions;
    }

    return make_tuple(averageRewards, percentageOptimals);
    //    return make_tuple(averageRewards, percentageOptimals);
}

class MP :public MatPlot{ 
void DISPLAY(){
    vector<double> x(100),y(100);    
    for(int i=0;i<100;++i){
	x[i]=0.1*i;
	y[i]=sin(x[i]);
    }
    plot(x,y);
}
}mp;
void display(){ mp.display(); }
void reshape(int w,int h){ mp.reshape(w,h); }
int main(int argc, char** argv) {
    /* initialize random seed: */
    srand(time(NULL));
    int expNo = 2;
    int plays = 3;
    double epsilons[3] = {0.1, 0.01, 0};
    double alpha = 0.1;
    double **averageRewards, **percentageOptimals;
    double *meanRewards[5], *meanPercentageOptimals[5];
    //run a experiment with each epsilon
    for (int i = 0; i < 3; i++) {
        tuple<double**, double**> outcome = runBanditExperience(expNo, plays, epsilons[i], 0.1);
        tie(averageRewards, percentageOptimals) = outcome;

        meanRewards[i] = xAxisMean(averageRewards, expNo, plays);
        meanPercentageOptimals[i] = xAxisMean(percentageOptimals, expNo, plays);

        //deallocate memory for past experiences
        for (int k = 0; k < expNo; ++k) {
            delete[] averageRewards[k];
            delete[] percentageOptimals[k];
        }
        delete[] averageRewards;
        delete[] percentageOptimals;

        cout << "Average Reward: ";
        printDoubleArray(meanRewards[i], plays);
    }

    //plotting
    glutInit(&argc, argv);
    glutCreateWindow(100,100,400,300);
    glutDisplayFunc( display );
    glutReshapeFunc( reshape );
    glutMainLoop();   
    
    return 0;
}

void printDoubleArray(double* arr, int n) {
    for (int i = 0; i < n; i++) {
        cout << arr[i] << ", ";
    }
    cout << endl;
}

double* xAxisMean(double** arr, int xn, int yn) {
    double* meanArr = new double[yn];
    for (int y = 0; y < yn; y++) {
        double sum = 0;
        for (int x = 0; x < xn; x++) {
            sum += arr[x][y];
        }
        meanArr[y] = sum / xn;
    }
    return meanArr;
}